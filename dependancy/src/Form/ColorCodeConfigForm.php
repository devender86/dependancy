<?php

namespace Drupal\dependancy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ColorCodeConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dependancy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dependancy.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dependancy.settings');

    $form['color_code'] = [
      '#type' => 'textarea',
      '#title' => $this
        ->t('Color'),
      '#description' => $this
        ->t('Enter the color code as key,value. Example: red|Red'),
      '#default_value' => !empty($config->get('color')) ? $config->get('color') : "",
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('dependancy.settings')
      // Set the submitted configuration setting.
      ->set('color', $form_state->getValue('color_code'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
