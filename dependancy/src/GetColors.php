<?php

namespace Drupal\drupal_di;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Get the Colors list from administration form.
 */
class GetColors {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a GetColor Object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Get the list of colors.
   */
  public function getColors() {
    // Get ColorCodeConfigForm Settings.
    $config = $this->configFactory->get('drupal_di.settings');
    $value = $config->get('color');
    // Explode the string by its new line.
    $color = $this->explodeArray("\n", $value);
    // Array of new colors to be included.
    $new_values = ["pink|Pink"];
    // Include the new colors.
    $included_colors_list = $this->includeNewColors($color, $new_values);
    // Iterating through all the colors.
    foreach ($included_colors_list as $value) {
      // Exploded the color string to get key value array.
      $res = $this->explodeArray("|", $value);
      $result[$res[0]] = $res[1];
    }
    return $result;
  }

  /**
   * Includes New colors to the array.
   *
   * @param array $color
   *   Array of colors.
   * @param array $new_values
   *   Array of new colors to be included.
   *
   * @return array
   *   Returns array of colors including pink color
   */
  public function includeNewColors(array $color, array $new_values) {
    foreach ($new_values as $new_val) {
      if (!in_array($new_val, $color)) {
        $color[] = $new_val;
      }
    }
    return $color;
  }

  /**
   * Explodes an array.
   *
   * @param string $delimiter
   *   Delimiter.
   * @param string $value
   *   Color.
   *
   * @return array
   *   Returns array of colors.
   */
  public function explodeArray(string $delimiter, string $value) {
    return explode($delimiter, $value);
  }

}
